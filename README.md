## lib_io

Collection of I/O functions.

## Install:

```shell
make
make install
```

## Usage:

1. Include header.

```C
#include "/path/to/lib_io.h"
```
2. Link lib_io.a.

```shell
gcc <your_source.c> lib_io.a
```
