#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "lib_io.h"

/* READING PARAMETERS */

struct params *param_list_create(char *filnam)
{
	char key[24] = {'\0'}, value[100] = {'\0'}, buffer[128], *comm;
	FILE *input;
	int i = 0;
	struct params *param_list;

	input = fopen(filnam, "r");
	assert(input);

	param_list = malloc(sizeof(struct params) * MAX_PARAMS);
	assert(param_list);

	while (fgets(buffer, 128, input)) {
		assert(i < MAX_PARAMS);
		if ((comm = strstr(buffer, "#")) != NULL) {
			buffer[comm - buffer] = (comm - buffer) ? '\n' : '\0';
			buffer[comm - buffer + 1] = '\0';
		}
		if (buffer[0] != '\n' && buffer[0] != '\0') {
			sscanf(buffer, "%s %s", &key[0], &value[0]);
			assert(key);
			assert(value);
			strncpy(param_list[i].key, key, 24UL);
			strncpy(param_list[i].value, value, 100UL);
			assert(param_list[i].value);
			i++;
		}
	}

	return param_list;
}

int param_int_lookup(struct params *param_list, char *key)
{
	int i;
	int value;
	int found = 0;

	for (i = 0; i < MAX_PARAMS; i++) {
		if (strcmp(key, param_list[i].key) == 0) {
			value = atoi(param_list[i].value);
			found = 1;
			break;
		}
	}

	if (found == 0) {
		fprintf(stderr, "%s NOT FOUND!\n", key);
		assert(found);
	}

	return value;
}

unsigned long param_ul_int_lookup(struct params *param_list, char *key)
{
	int i;
	unsigned long value;
	int found = 0;

	for (i = 0; i < MAX_PARAMS; i++) {
		if (strcmp(key, param_list[i].key) == 0) {
			value = atoi(param_list[i].value);
			found = 1;
			break;
		}
	}

	if (found == 0) {
		fprintf(stderr, "%s NOT FOUND!\n", key);
		assert(found);
	}

	return value;
}

double param_double_lookup(struct params *param_list, char *key)
{
	int i;
	double value;
	int found = 0;

	for (i = 0; i < MAX_PARAMS; i++) {
		if (strcmp(key, param_list[i].key) == 0) {
			value = atof(param_list[i].value);
			found = 1;
			break;
		}
	}

	if (found == 0) {
		fprintf(stderr, "%s NOT FOUND!\n", key);
		assert(found);
	}

	return value;
}

int param_string_lookup(struct params *param_list, char *key, char *value)
{
	int i;
	int found = 0;

	for (i = 0; i < MAX_PARAMS; i++) {
		if (strcmp(key, param_list[i].key) == 0) {
			strncpy(value, param_list[i].value, 100UL);
			found = 1;
			break;
		}
	}
	if (found == 0) {
		fprintf(stderr, "%s NOT FOUND!\n", key);
		assert(found);
	}

	assert(value);

	return 0;
}

void param_list_print(char *file, struct params *param_list)
{
	FILE *outfile;
	int i = 0;

	outfile = fopen(file, "w");
	assert(outfile);

	while (param_list[i].key[0] != '\0') {
		fprintf(outfile, "## %s = %s\n", param_list[i].key, param_list[i].value);
		i++;
	}

	fclose(outfile);
}

void param_list_destroy(struct params *param_list) { free(param_list); }

/* READING DATA */

double *read_file(char *file, unsigned long *row, unsigned long *column)
{
	const char delim[] = " \t";
	char *token, *linebuffer, *ret;
	int i = 0;
	unsigned long rows = 0, columns = 0;
	double *data;
	FILE *fp;

	fp = fopen(file, "r");
	assert(fp);
	linebuffer = malloc(MAX_LINE_LENGTH * sizeof(char));
	assert(linebuffer);
	ret = fgets(linebuffer, MAX_LINE_LENGTH, fp);

	/* COUNTING LINES */
	while (ret) {
		token = strtok(linebuffer, delim);
		if (token[0] != '#' && token[0] != '\n' && token[0] != ' ' && token[0] != '\t') {
			rows++;
		}
		ret = fgets(linebuffer, MAX_LINE_LENGTH, fp);
	}

	/* COUNTING COLUMNS */
	fseek(fp, 0, SEEK_SET);
	ret = fgets(linebuffer, MAX_LINE_LENGTH, fp);

	while (ret) {
		token = strtok(linebuffer, delim);
		while (token) {
			if (token[0] != '#' && token[0] != '\n' && token[0] != ' '
			    && token[0] != '\t') {
				columns++;
			} else {
				break;
			}
			token = strtok(NULL, delim);
		}
		if (columns) {
			break;
		}
		ret = fgets(linebuffer, MAX_LINE_LENGTH, fp);
	}

	fseek(fp, 0, SEEK_SET);

	/* LOADING DATA INTO AN ARRAY */
	data = malloc(rows * columns * sizeof(double));
	assert(data);
	ret = fgets(linebuffer, MAX_LINE_LENGTH, fp);

	while (ret) {
		token = strtok(linebuffer, delim);
		while (token) {
			if (token[0] != '#' && token[0] != '\n' && token[0] != ' '
			    && token[0] != '\t') {
				data[i++] = atof(token);
			} else {
				break;
			}
			token = strtok(NULL, delim);
		}
		ret = fgets(linebuffer, MAX_LINE_LENGTH, fp);
	}

	free(linebuffer);
	fclose(fp);
	*row = rows;
	*column = columns;

	return data;
}

int get_size(const double *D, unsigned long r_D, unsigned long c_D, unsigned long *nX,
	     unsigned long *nY)
{

	unsigned long n_K = 1, n_X, i = 0;

	while (D[i * c_D] == D[(i + 1) * c_D] && i < r_D) {
		n_K++;
		i++;
	}

	*nY = n_K;
	n_X = r_D / n_K;
	*nX = n_X;

	return 0;
}

int splice(const double *D, unsigned long c_D, unsigned long nX, unsigned long nY, double *K,
	   double *V)
{
	unsigned long i, j, l;

	for (i = 0; i < nY; i++) {
		K[i] = D[i * c_D + 1];
	}

	for (i = 0; i < nX; i++) {
		for (j = 0; j < nY; j++) {
			for (l = 0; l < c_D - 2; l++) {
				V[j + i * nY + l * nX * nY] = D[c_D * (i * nY + j) + 2 + l];
			}
		}
	}

	return 0;
}

int read_pot(char *file, double **k, unsigned long *nk, double **v, unsigned long *nv)
{
	double *D;
	unsigned long nrow, ncol, nx, ny, nnk, nnv;

	D = read_file(file, &nrow, &ncol);
	assert(D);
	get_size(D, nrow, ncol, &nx, &ny);

	assert(nx == ny);
	nnk = nx;

	nnv = nnk * nnk * (ncol - 2);

	*k = malloc(nnk * sizeof(double));
	assert(*k);
	*v = malloc(nnv * sizeof(double));
	assert(*v);

	*nk = nnk;
	*nv = nnv;

	splice(D, ncol, nx, ny, *k, *v);
	free(D);

	return 0;
}

int read_mesh(char *file, double **x, double **w, unsigned long *nmesh)
{
	double *mesh;
	unsigned long i, nr, ncol;

	mesh = read_file(file, &nr, &ncol);
	assert(mesh);

	*x = malloc(nr * sizeof(double));
	assert(*x);
	*w = malloc(nr * sizeof(double));
	assert(*w);

	for (i = 0; i < nr; i++) {
		(*x)[i] = mesh[i * 2];
		(*w)[i] = mesh[i * 2 + 1];
	}

	free(mesh);
	*nmesh = nr;

	return 0;
}

int print_pot(FILE *file, double *k, unsigned long nk, double *v, unsigned long nv)
{
	unsigned long i, j, l, ncol;

	ncol = nv / (nk * nk);

	for (i = 0; i < nk; i++) {
		for (j = 0; j < nk; j++) {
			fprintf(file, "%+.15E %+.15E", k[i], k[j]);

			for (l = 0; l < ncol; l++)
				fprintf(file, " %+.15E", v[i * nk + j + l * nk * nk]);

			fprintf(file, "\n");
		}
		fprintf(file, "\n");
	}
	return 0;
}
