#include <stdio.h> /* FOR fprintf in param_list_print */

#define MAX_PARAMS 50
#define MAX_LINE_LENGTH 256

/* DATATYPES DEFINITIONS */

struct params {
	char key[24], value[100];
};

/* FUNCTION DECLARATIONS */

struct params *param_list_create(char *filnam);
char *param_list_lookup(char *key, struct params *param_list);
int param_string_lookup(struct params *param_list, char *key, char *value);
int param_int_lookup(struct params *param_list, char *key);
unsigned long param_ul_int_lookup(struct params *param_list, char *key);
double param_double_lookup(struct params *param_list, char *key);
void param_list_print(char *file, struct params *param_list);
void param_list_destroy(struct params *param_list);

double *read_file(char *file, unsigned long *row, unsigned long *column);
int get_size(const double *D, unsigned long r_D, unsigned long c_D, unsigned long *nX,
	     unsigned long *nY);
int splice(const double *D, unsigned long c_D, unsigned long nX, unsigned long nY, double *K,
	   double *V);
int read_pot(char *file, double **k, unsigned long *nk, double **v, unsigned long *nv);
int read_mesh(char *file, double **x, double **w, unsigned long *nmesh);
int print_pot(FILE *file, double *k, unsigned long nk, double *v, unsigned long nv);
